from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from . import views


# Create your tests here.

class HomeTest(TestCase):
	def test_home_is_there(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_home_is_valid(self):
		res = resolve('/')
		self.assertEqual(res.func,  views.home)

	def test_home_correct_template(self):
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'landing.html')
