from django.urls import path
from .views import *
from . import views

app_name='testimoni'
urlpatterns = [
	
	path('',views.view_testimoni, name="view_testimoni"),
	path('api/testimoni/', views.TestimoniList.as_view(), name=("view-api")),
	path('api/testimoni/create', views.TestimoniCreateList.as_view(), name=("view-api"))
	]