from django.test import TestCase, Client
from django.urls import resolve
from .views import add_payments, get_payments
from .models import Model_form
from .forms import PaymentForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time 
from selenium.webdriver import ActionChains
from django.contrib.auth.models import User
from django.contrib import auth
# Create your tests here.
class TestURLDanView(TestCase):
	def test_url_payments_ada(self):
		response = Client().get('/payments/')
		self.assertEqual(response.status_code, 200)
	def test_template_benar_ga(self):
		response = Client().get('/payments/')
		self.assertTemplateUsed(response,'payments.html')
	def test_pake_fungsi_isi_pembayaran(self):
		fungsi = resolve('/payments/')
		self.assertEqual(fungsi.func, add_payments)
	def test_pake_fungsi_get_payments(self):
		fungsi = resolve('/payments/dataPayments/')
		self.assertEqual(fungsi.func, get_payments)
	def test_url_payments_ada_data(self):
		response = Client().get('/payments/dataPayments/')
		self.assertEqual(response.status_code, 200)
class TestModel(TestCase): 
	@classmethod
	def setUpTestData(cls):
		Model_form.objects.create(nama_pemegang_kartu='Llala lili', ccv='123',
			nomor_kartu_debit='123456789112334',kadaluarsa='2020-12-11')
	def test_create_object(self):
		count = Model_form.objects.all().count()
		self.assertEqual(count,1)
	def test_nama_pemegang_kartu_max_length(self): 
		obj = Model_form.objects.get(id=1)
		max_length = obj._meta.get_field('nama_pemegang_kartu').max_length
		self.assertEqual(max_length,50)
	def test_digit_kartu_kredit_max_length(self):			
		obj = Model_form.objects.get(id=1)
		max_length = obj._meta.get_field('nomor_kartu_debit').max_length
		self.assertEqual(max_length,16)
	def test_digit_ccv_max_length(self):
		obj = Model_form.objects.get(id=1)
		max_length = obj._meta.get_field('ccv').max_length
		self.assertEqual(max_length,4)
	def test_nama_pemegang_kartu_sebagai_str(self):
		obj = Model_form.objects.get(id=1)
		expect = obj.nama_pemegang_kartu
		self.assertEqual(expect, str(obj))
class TestForm(TestCase):
	def test_apakah_ke_post(self):
		response = self.client.post('/payments/', data={
			'nama_pemegang_kartu':'Llala lili', 'ccv':'123',
			'nomor_kartu_debit':'123456789112334','kadaluarsa':'2020-12-11'
			})
		self.assertEqual(response.status_code, 302)
		form = PaymentForm(data={
			'nama_pemegang_kartu':'Llala lili', 'ccv':'123',
			'nomor_kartu_debit':'123456789112334','kadaluarsa':'2020-12-11'
			})
		self.assertTrue(form.is_valid())
		response_get = self.client.get('/payments/')
		html_response = response.content.decode('utf8')
		self.assertIn(html_response, 'Llala lili')
	def test_apakah_gak_ke_post(self):
		response = self.client.post('/payments/', data={
			'nama_pemegang_kartu':'Llala lili', 'ccv':'12355',
			'nomor_kartu_debit':'123456789112334','kadaluarsa':'2020-12-11'
			})
		self.assertEqual(response.status_code, 200)
		form = PaymentForm(data={
			'nama_pemegang_kartu':'Llala lili', 'ccv':'12355',
			'nomor_kartu_debit':'123456789112334','kadaluarsa':'2020-12-11'
			})
		self.assertFalse(form.is_valid())
class FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser = webdriver.Chrome(executable_path='./chromedriver',chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()
	def tearDowm(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()
	def test_mousehover(self):
		response = self.client.post('https://petcarepw.herokuapp.com/accounts/login/', data={'username':'sitisk', 'password':'sitisk'})
		exist = auth.get_user(self.client)
		if(exist.is_authenticated):
			self.browser.get('https://petcarepw.herokuapp.com/payments/')
			time.sleep(3)
			field1 = self.browser.find_element_by_id('id_nama_pemegang_kartu')
			field2= self.browser.find_element_by_id('id_nomor_kartu_kredit')
			field3 = self.browser.find_element_by_id('id_kadaluarsa')
			field4 = self.browser.find_element_by_id('id_ccv')
			hover1 = ActionChains(self.browser).move_to_element(field1)
			hover2 = ActionChains(self.browser).move_to_element(field2)
			hover3 = ActionChains(self.browser).move_to_element(field3)
			hover4 = ActionChains(self.browser).move_to_element(field4)
			hover1.perform()
			hover2.perform()
			hover3.perform()
			hover4.perform()
			border1 = field1.value_of_css_property('border')
			border2 = field2.value_of_css_property('border')
			border3 = field3.value_of_css_property('border')
			border4 = field4.value_of_css_property('border')
			self.assertTrue(border1, '1px solid #ced4da')
			self.assertTrue(border2, '1px solid #ced4da')
			self.assertTrue(border3, '1px solid #ced4da')
			self.assertTrue(border4, '1px solid #ced4da')
		else:
			res = self.client.get('/payments/')
			html_response = res.content.decode('utf8')
			self.assertIn("Silahkan login terlebih dahulu dan lakukan pemesanan jasa",html_response)
			# self.browser.get('https://petcarepw.herokuapp.com/payments/')
			# login = self.browser.find_element_by_id('login')
			# login.send_keys(Keys.RETURN)

	def test_input(self):
		response = self.client.post('https://petcarepw.herokuapp.com/accounts/login/', data={'username':'sitisk', 'password':'sitisk'})
		exist = auth.get_user(self.client)
		if(exist.is_authenticated):
			self.browser.get('https://petcarepw.herokuapp.com/payments/')
			time.sleep(3)
			field1 = self.browser.find_element_by_id('id_nama_pemegang_kartu')
			time.sleep(2)
			field2= self.browser.find_element_by_id('id_nomor_kartu_kredit')
			time.sleep(2)
			field3 = self.browser.find_element_by_id('id_kadaluarsa')
			time.sleep(2)
			field4 = self.browser.find_element_by_id('id_ccv')
			time.sleep(2)
			button = self.browser.find_element_by_id('button_konfirmasi')
			disable = button.value_of_css_property('disabled')
			field1.send_keys('Coba Coba')
			field2.send_keys('123')
			field3.send_keys('20-1-2019')
			field4.send_keys('123')
			button.send_keys(Keys.RETURN)
			self.assertEqual(disable, '')
		else:
			res = self.client.get('/payments/')
			html_response = res.content.decode('utf8')
			self.assertIn("Silahkan login terlebih dahulu dan lakukan pemesanan jasa",html_response)