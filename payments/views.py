from django.shortcuts import render, redirect
from .models import Model_form
from .forms import PaymentForm
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
# Create your views here.

def add_payments(request):
	if request.method =='POST':
		form = PaymentForm(request.POST)
		if form.is_valid():
			post = form.save(commit=False) 
			post.save()
			return redirect('/payments/')
	else:
		form = PaymentForm()
	#ambil object terakhir aja
	objects = Model_form.objects.order_by().last()
	isi = {'generate_form': form,
			'all_objects':objects}
	return render(request, 'payments.html', isi)
def get_payments(request):
	if(request.method == "GET"):
		data = Model_form.objects.all();
		data_payments = []
		for i in data:
			data_payments.append({
				'nama': i.nama_pemegang_kartu,
				'nomor_kartu': i.nomor_kartu_debit,
				'kadaluarsa':i.kadaluarsa,
				'ccv':i.ccv,
			})
		result = json.dumps(data_payments, cls=DjangoJSONEncoder)
		return HttpResponse(content=result, content_type = "application/json")
	return HttpResponse("[]")

