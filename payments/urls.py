from django.urls import path
from . import views

app_name = 'payments'

urlpatterns = [
		path('', views.add_payments, name='add_payments'),
		path('dataPayments/', views.get_payments, name='get_payments')
]