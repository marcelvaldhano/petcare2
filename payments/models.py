from django.db import models
from django.core.validators import MinLengthValidator
# Create your models here.

class Model_form(models.Model):
	nama_pemegang_kartu = models.TextField(max_length=50)
	nomor_kartu_debit = models.TextField(max_length=16, validators=[MinLengthValidator(15)])
	kadaluarsa = models.DateField()
	ccv = models.TextField(max_length=4, validators=[MinLengthValidator(3)])
	def __str__(self):
		return self.nama_pemegang_kartu
