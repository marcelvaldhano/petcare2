from django import forms
import datetime
class Message_Form(forms.Form):
        service = forms.CharField(label='', required=True, 
                max_length=90, 
                widget=forms.TextInput(attrs = {"class": "form-control", "placeholder" : "Select Services", "id" : "service"}))
        doctor = forms.CharField(label='', required=True, 
                widget=forms.TextInput(attrs = {"class": "form-control", "placeholder" : "Select Doctors", "id" : "doctor"}))
        fullname = forms.CharField(label='', required=True, 
                widget=forms.TextInput(attrs = {"class": "form-control", "placeholder" : "Your Full Name", "id" : "fullname"}))
        phone = forms.CharField(label='', required=True, 
                widget=forms.TextInput(attrs = {"class": "form-control", "placeholder" : "Phone", "id" : "phone"}))
        date = forms.DateField(label='', initial=datetime.date.today, 
                required=True, widget=forms.DateTimeInput(attrs = {"class": "form-control", "type" : "date", "id" : "date"}))


