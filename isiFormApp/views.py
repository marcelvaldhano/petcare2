from django.shortcuts import render
from datetime import datetime, date
from .forms import *
from .models import *
import json
from django.http import HttpResponse, JsonResponse



# Create your views here.

def isiForm(request):
    form = Message_Form()
    bookings = Bookings.objects.all()
    response = {'message_form': form, 'myBookings' : bookings}
    return render(request, 'isiForm.html', response)


def create_post(request):
    if request.method == 'POST':
        service = request.POST.get('service')
        doctor = request.POST.get('doctor')
        fullname = request.POST.get('fullname')
        phone = request.POST.get('phone')
        date = request.POST.get('date')
        response_data = {}

        post = Bookings(service=service, doctor=doctor, fullname=fullname, phone=phone, date=date)
        post.save()

        response_data['result'] = 'Create post successful!'
        response_data['postpk'] = post.pk
        response_data['service'] = post.service
        response_data['doctor'] = post.doctor
        response_data['fullname'] = post.fullname
        response_data['phone'] = post.phone
        response_data['date'] = post.date

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )

def getAllData(request):
    bookings = list(Bookings.objects.values())
    data = {
        'bookings': bookings
    }
    return JsonResponse(data)
