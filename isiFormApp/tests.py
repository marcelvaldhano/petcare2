from django.test import TestCase
from django.test import Client
from .views import *
from .models import *
from .forms import *
from django.urls import resolve


# Create your tests here.
class UnitTest(TestCase):
    
    def test_url_isiForm_exist(self):
        response = Client().get('/isiForm/')
        self.assertEqual(response.status_code, 200)
    def test_url_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    def test_form_status_html(self):
        form = Message_Form()
        self.assertIn('class="form-control', form.as_p())
        self.assertIn('id="service', form.as_p())
        self.assertIn('id="doctor', form.as_p())
        self.assertIn('id="fullname', form.as_p())
        self.assertIn('id="phone', form.as_p())
        self.assertIn('id="date', form.as_p())

    def test_view_isiForm(self):
        found = resolve('/isiForm/')
        self.assertEqual(found.func, isiForm)

    def test_view_create_post(self):
        found = resolve('/isiForm/create_post')
        self.assertEqual(found.func, create_post)

    def test_create_success(self):
        response_post = Client().post('/isiForm/create_post', {'service': 'Test', 'doctor': 'Testi', 'fullname': 'Test123','phone':'012345678', 'date':'1111-11-11'})
        self.assertEqual(response_post.status_code, 200)

    def test_getAllData_success(self):
        response_post = Client().get('/isiForm/getAllData')
        self.assertEqual(response_post.status_code, 200)

    def test_view_getAllData(self):
        found = resolve('/isiForm/getAllData')
        self.assertEqual(found.func, getAllData)

    def test_model_Bookings(self): 
        record = Bookings.objects.create(date="2000-09-07", service="nails", doctor="dhafin", fullname="razaafDhafin", phone="999999")
        count_records = Bookings.objects.all().count()
        self.assertEqual(count_records, 1)
